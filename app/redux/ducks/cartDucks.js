
// action 
export const ADD_PRODUCT_CART = 'cartDucks/ADD_PRODUCT_CART'
export const DELETE_ITEM = 'cartDucks/DELETE_ITEM'
export const RESET_CART = 'cartDucks/RESET_CART'


export const addProductCart = (data) => ({
    type: ADD_PRODUCT_CART,
    payload: data
})

export const deleteItem = (name) => ({
    type: DELETE_ITEM,
    payload: name
})

export const resetCart = () => ({
    type: RESET_CART
})
//initial state
const initialState = []

// reducer
export default (state = initialState, action) => {
    switch (action.type) {

        case ADD_PRODUCT_CART:
            let auth = false 
            for(let elem in state ){
                console.log( action.payload)
                if(state[elem][0] == action.payload[0]){
                    auth = true
                }
                
            }
           if(auth){
               return state
           }else{
            return [...state,  action.payload ]
           }

        case DELETE_ITEM:
            return state.filter(elem => (
                elem[0] != action.payload
            ))

        case RESET_CART:
            return []

        default:
            return state
    }
}
