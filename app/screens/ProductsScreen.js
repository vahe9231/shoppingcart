import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet } from 'react-native'
import ProductCard from '../components/ProductCard'
import  * as fakeApi from '../api/fakeApi'



export default class ProductsScreen extends Component {
  render() {
    return (
      <View>
        <ScrollView style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        {
          fakeApi.products.map((elem,index) => (
            <ProductCard variant = {elem.variant} limit = {elem.limit} key = {index} name ={elem.name}/>
          ))
        }
      

      </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container:{
      margin:10
    }
  });
  