import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import { connect } from 'react-redux'
import { deleteItem, resetCart } from '../redux/ducks/cartDucks'
import CartAlert from '../components/CartAlert'

class CartScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['name', 'variant', 'count', 'addition', 'addcount']

        }
    }

    _alertIndex(index) {
        console.log(index);
    }
    resetCart = () => {
        CartAlert("Reset ", "Do you want to delete the whole basket?", this.props.resetCart)
    }
    render() {

        const state = this.state;
        const element = (data, index) => (
            <TouchableOpacity onPress={() => this._alertIndex(data)}>
                <View style={styles.btn}>
                    <Text style={styles.btnText}>button</Text>
                </View>
            </TouchableOpacity>
        );

        return (
            <View style={styles.container}>
                <Table borderStyle={{ borderColor: 'transparent' }}>
                    <Row data={state.tableHead} style={styles.head} textStyle={styles.text} />
                    {
                        this.props.cartR.map((rowData, index) => (
                            <TableWrapper key={index} style={styles.row} >
                                {

                                    rowData.map((cellData, cellIndex) => (
                                        <Cell key={cellIndex} data={cellIndex === 6 ? element(cellData, index) : cellData}
                                            textStyle={styles.text}
                                            onPress={() => CartAlert("Delete Item",
                                                "Do you want to delete this item?",
                                                () => this.props.deleteItem(this.props.cartR[index][0]))
                                            } />
                                    ))
                                }
                            </TableWrapper>
                        ))
                    }
                </Table>
                <TouchableOpacity
                    onPress={this.resetCart}
                >
                    <Text>Reset cart</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingTop: 40,
        backgroundColor: '#fff'
    },
    head: {
        height: 40,
        backgroundColor: '#808B97'
    },
    text: {
        margin: 6
    },
    row: {
        flexDirection: 'row',
        backgroundColor: '#FFF1C1'
    },
    btn: {
        width: 58,
        height: 18,
        backgroundColor: '#78B7BB',
        borderRadius: 2
    },
    btnText: {
        textAlign: 'center',
        color: '#fff'
    }
});

export default connect(state => ({
    cartR: state.cartR
}), { deleteItem, resetCart })(CartScreen)