import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'
import RadioButton from 'radio-button-react-native'
import { connect } from 'react-redux'
import { addProductCart } from '../redux/ducks/cartDucks'


class ProductCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            size: 'big',
            count: 1,
            addition: "Fanta",
            additionCount: 1
        }
    }
    handleSizeChange = (value) => {
        this.setState({ size: value })
    }

    incrementCount = () => {
        const { count } = this.state
        console.log(count)
        if (count < this.props.limit) {
            this.setState({ count: ++this.state.count })
        }
    }

    decrementCount = () => {
        const { count } = this.state
        console.log(count)
        if (count > 0) {
            this.setState({ count: --this.state.count })
        }
    }

    incrementAdditionCount = () => {

        this.setState({ additionCount: ++this.state.additionCount })

    }

    decrementAdditionCount = () => {
        const { additionCount } = this.state
        if (additionCount > 1) {
            this.setState({ additionCount: --this.state.additionCount })
        }
    }

    handleAddCart = () => {
        this.props.addProductCart([
            this.props.name,
            this.state.size,
            this.state.count,
            this.state.additionCount,
            this.state.addition,
        ])
    }
    handleChangeAdditions = (value) => {
        this.setState({ addition: value })
    }
    render() {
        console.log(this.props)
        return (
            <View style={styles.container}>
                {/* product name */}
                <Text style={styles.title}> {this.props.name} </Text>
                {
                    /* select size */
                    !!this.props.variant &&
                    <View style={styles.checkBoxContainer}>
                        <RadioButton currentValue={this.state.size} value='big' onPress={this.handleSizeChange}>
                            <Text>Big</Text>
                        </RadioButton>

                        <RadioButton currentValue={this.state.size} value='medium' onPress={this.handleSizeChange}>
                            <Text>Medium</Text>
                        </RadioButton>

                        <RadioButton currentValue={this.state.size} value='small' onPress={this.handleSizeChange}>
                            <Text>Small</Text>
                        </RadioButton>

                    </View>
                }
                {
                    /*  limit product */

                    <View style={styles.counterContainer}>
                        <TouchableOpacity
                            onPress={this.incrementCount}
                            style={styles.counterButton}>
                            <Text style={{ fontSize: 20 }}>+</Text>
                        </TouchableOpacity>

                        <Text style={{ fontSize: 20 }}>{this.state.count}</Text>
                        <TouchableOpacity
                            onPress={this.decrementCount}
                            style={styles.counterButton}>
                            <Text style={{ fontSize: 20 }}>-</Text>
                        </TouchableOpacity>
                    </View>

                }
                {/*   select addition */}
                <View>
                    <View style={styles.checkBoxContainer}>
                        <RadioButton currentValue={this.state.addition} value='Coca Cola' onPress={this.handleChangeAdditions}>
                            <Text>Coca Cola</Text>
                        </RadioButton>

                        <RadioButton currentValue={this.state.addition} value='Fanta' onPress={this.handleChangeAdditions}>
                            <Text>Fanta</Text>
                        </RadioButton>
                    </View>
                    {/* addition count */}
                    <View style={styles.counterContainer}>
                        <TouchableOpacity
                            onPress={this.incrementAdditionCount}
                            style={styles.counterButton}>
                            <Text style={{ fontSize: 20 }}>+</Text>
                        </TouchableOpacity>

                        <Text style={{ fontSize: 20 }}>{this.state.additionCount}</Text>
                        <TouchableOpacity
                            onPress={this.decrementAdditionCount}
                            style={styles.counterButton}>
                            <Text style={{ fontSize: 20 }}>-</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <TouchableOpacity
                    style={{ backgroundColor: '#afa', width: '30%', alignSelf: 'center', marginTop: 10 }}
                    onPress={this.handleAddCart}>
                    <Text style={{ textAlign: 'center' }} >
                        Add to cart
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        textAlign: 'center',
        padding: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        marginTop: 3,
        backgroundColor: '#eee'
    },
    title: {
        alignSelf: 'center',
        lineHeight: 40
    },
    checkBoxContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: 320
    },
    counterContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#07a8',
        alignItems: 'center',
        padding: 5,
        margin: 10
    },
    counterButton: {

        textAlign: 'center'
    }
})


export default connect(state => ({
    cartR: state.cartR
}), { addProductCart })(ProductCard)