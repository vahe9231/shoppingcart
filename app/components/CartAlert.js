import { Alert } from 'react-native';

const CartAlert = (title,message,actionOK) => {
    Alert.alert(
        title,
        message,
        [  
          {text: 'Cancel', onPress: () => console.log(""),style: 'cancel'},
          {text: 'delete', onPress: () => actionOK()},
        ],
        { cancelable: true }
      )
}

export default CartAlert;