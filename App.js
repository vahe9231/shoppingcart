/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import {  createBottomTabNavigator } from 'react-navigation'

import ProductScreen from './app/screens/ProductsScreen'
import CartScreen from './app/screens/CartScreen'

const TabRoot = createBottomTabNavigator({
  Product:{
    screen:ProductScreen
  },
  Cart:{
    screen:CartScreen
  }

})

export default class App extends Component {
  render() {
    return (
      <TabRoot />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
